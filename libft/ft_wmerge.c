/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wmerge.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 17:33:49 by clrichar          #+#    #+#             */
/*   Updated: 2018/01/27 17:33:49 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

wchar_t				*ft_wmerge(wchar_t *ws, char *s)
{
	int			i;
	int			j;
	wchar_t		*wret;

	if (!s || !ws)
		return (NULL);
	if (!(wret = ft_wstrnew((ft_strlen(s) + ft_wstrlen(ws)) + 1)))
		return (NULL);
	i = 0;
	j = 0;
	while (ws[i])
	{
		wret[i] = ws[i];
		i++;
	}
	while (s[j])
	{
		wret[i + j] = s[j];
		j++;
	}
	wret[i + j] = L'\0';
	ft_strdel(&s);
	ft_wstrdel(&ws);
	return (wret);
}
