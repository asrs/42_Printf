/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub_free.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 17:33:48 by clrichar          #+#    #+#             */
/*   Updated: 2018/01/27 17:33:48 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

char			*ft_strsub_free(char *s, unsigned int start, size_t len)
{
	size_t		i;
	char		*dest;

	if (!s)
		return (NULL);
	if (!(dest = ft_strnew(len)))
		return (NULL);
	i = 0;
	while (*(s + (start + i)) && i < len)
	{
		*(dest + i) = *(s + (start + i));
		i++;
	}
	free((void *)s);
	return (dest);
}
