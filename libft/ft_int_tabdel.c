/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_int_tabdel.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/16 18:28:31 by clrichar          #+#    #+#             */
/*   Updated: 2018/02/17 14:13:01 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void				ft_int_tabdel(int **as)
{
	int				i;

	if (as == NULL)
		return ;
	i = 0;
	while ((*as)[i])
	{
		if ((*as)[i])
		{
			free(&(*as)[i]);
		}
		i++;
	}
	free((*as));
	(*as) = NULL;
}
