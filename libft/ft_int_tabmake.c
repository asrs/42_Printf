/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_int_tabmake.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 17:33:35 by clrichar          #+#    #+#             */
/*   Updated: 2018/01/27 17:33:35 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int				*ft_int_tabmake(size_t size)
{
	int			*tab;
	size_t		i;

	if (!(tab = (int *)malloc(sizeof(int) * size)))
		return (NULL);
	i = 0;
	while (i < size)
		tab[i++] = 0;
	return (tab);
}
