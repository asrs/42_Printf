/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 17:33:51 by clrichar          #+#    #+#             */
/*   Updated: 2018/01/27 17:33:51 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int			ft_slice(char **line, char **post)
{
	int				len;

	len = 0;
	if (ft_strchr(*post, '\n'))
	{
		len = ft_strlenc(*post, '\n');
		*line = ft_strsub(*post, 0, (size_t)len);
		*post = ft_strsub_free(*post, (unsigned int)len + 1,
				(size_t)ft_strlen(*post));
		return (1);
	}
	else if (ft_strlen(*post) > 0 && !(ft_strchr(*post, '\n')))
	{
		*line = ft_strdup(*post);
		ft_strdel(post);
		return (1);
	}
	return (0);
}

int					get_next_line(const int fd, char **line)
{
	long int		rd;
	char			buffer[BUFF_SIZE + 1];
	static char		*post = NULL;

	if (fd < 0 || !line || BUFF_SIZE == 0)
		return (-1);
	rd = 0;
	if ((rd = read(fd, buffer, BUFF_SIZE)) > 0)
	{
		buffer[rd] = '\0';
		if (post)
			post = ft_strjoin_free(post, buffer, 1);
		else
			post = ft_strdup(buffer);
		if (!(ft_strchr(buffer, '\n')))
			return (get_next_line(fd, line));
	}
	if (((post) && (ft_slice(line, &post))) || (rd > 0))
		return (1);
	else if (rd <= 0 && (post))
	{
		*line = ft_strdup(post);
		ft_strdel(&post);
	}
	return ((rd == -1) ? -1 : 0);
}
