# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: clrichar <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/02/20 15:08:30 by clrichar          #+#    #+#              #
#                                                                              #
#    Updated: 2018/02/26 13:49:27 by clrichar         ###   ########.fr        #
# **************************************************************************** #

NAME			:=			libftprintf.a

#==============================================================================#
#------------------------------------------------------------------------------#
#                               DIRECTORIES                                    #

SRC_DIR			:=			./srcs
INC_DIR			:=			./includes
OBJ_DIR			:=			./obj
LIB_DIR			:=			./libft

#==============================================================================#
#------------------------------------------------------------------------------#
#                                  FILES                                       #

SRC				:=			ft_printf.c					\
							ft_parse.c					\
							ft_color.c					\
							ft_flag.c					\
							ft_buffer.c					\
							m_get.c						\
							m_set.c						\
							m_int.c						\
							m_uint.c					\
							m_char.c					\
							m_string.c					\
							m_misc.c					\
							is_file.c					\
							utils.c						\

OBJ				:=			$(addprefix $(OBJ_DIR)/,$(SRC:.c=.o))
NB				:=			$(words $(SRC))
INDEX			:=			0

LIBSRC			:=			ft_putchar.c				\
							ft_putstr.c					\
							ft_putendl.c				\
							ft_putnbr.c					\
							ft_putchar_fd.c				\
							ft_putstr_fd.c				\
							ft_putendl_fd.c				\
							ft_putnbr_fd.c				\
							ft_toupper.c				\
							ft_tolower.c				\
							ft_isascii.c				\
							ft_isalpha.c				\
							ft_isdigit.c				\
							ft_isalnum.c				\
							ft_isprint.c				\
							ft_atoi.c					\
							ft_atoi_base.c				\
							ft_strlen.c					\
							ft_strdup.c					\
							ft_strcpy.c					\
							ft_strncpy.c				\
							ft_strcat.c					\
							ft_strncat.c				\
							ft_strchr.c					\
							ft_strrchr.c				\
							ft_strnchr.c				\
							ft_strstr.c					\
							ft_strnstr.c				\
							ft_strcmp.c					\
							ft_strncmp.c				\
							ft_strnew.c					\
							ft_strdel.c					\
							ft_strclr.c					\
							ft_str_tolower.c			\
							ft_memset.c					\
							ft_memalloc.c				\
							ft_memdel.c					\
							ft_bzero.c					\
							ft_memcpy.c					\
							ft_memccpy.c				\
							ft_memmove.c				\
							ft_memchr.c					\
							ft_memcmp.c					\
							ft_itoa.c					\
							ft_itoa_base.c				\
							ft_utoa_base.c				\
							ft_striter.c				\
							ft_striteri.c				\
							ft_strmap.c					\
							ft_strmapi.c				\
							ft_strequ.c					\
							ft_strnequ.c				\
							ft_strsub.c					\
							ft_strjoin.c				\
							ft_strtrim.c				\
							ft_strsplit.c				\
							ft_strjoin_free.c			\
							ft_strsub_free.c			\
							ft_countword.c				\
							ft_intlen.c					\
							ft_int_tabmake.c			\
							ft_int_tabdel.c				\
							ft_int_puttab.c				\
							ft_str_tabmake.c			\
							ft_str_puttab.c				\
							ft_str_tabdel.c				\
							ft_strrev.c					\
							ft_isspace.c				\
							ft_sqrt.c					\
							ft_swap_char.c				\
							ft_swap_tab.c				\
							ft_strlenc.c				\
							ft_wstrlen.c				\
							ft_wstrnew.c				\
							ft_wmerge.c					\
							ft_wstrcat.c				\
							ft_wstrjoin.c				\
							ft_wstrjoin_free.c			\
							ft_wstrdup.c				\
							ft_wstrdel.c				\
							ft_wputchar.c				\
							ft_wputstr.c				\
							ft_printbits.c				\
							get_next_line.c				\

LIBOBJ			:=			$(addprefix $(LIB_DIR)/,$(LIBSRC:.c=.o))

#==============================================================================#
#------------------------------------------------------------------------------#
#                            COMPILER & FLAGS                                  #

CC				:=			gcc
CFLAGS			:=			-Wall -Wextra -Werror -o2 -g3
OFLAGS			:=			-pipe
CFLAGS			+=			$(OFLAGS)
CLIB			:=			-L$(LIB_DIR) -lft

#==============================================================================#
#------------------------------------------------------------------------------#
#                                LIBRARY                                       #

L_FT			:=			$(LIB_DIR)

#==============================================================================#
#------------------------------------------------------------------------------#
#                                 RULES                                        #

all:					$(NAME)


$(NAME):				lib obj_inc $(OBJ)
	@ar rc $(NAME) $(OBJ) $(LIBOBJ)
	@ranlib $(NAME)
	@printf '\033[33m[ 100%% ] %s\n\033[0m' "Compilation of $(NAME) is done ---"


$(OBJ_DIR)/%.o:			$(SRC_DIR)/%.c
	@$(eval DONE=$(shell echo $$(($(INDEX)*20/$(NB)))))
	@$(eval PERCENT=$(shell echo $$(($(INDEX)*100/$(NB)))))
	@$(eval TO_DO=$(shell echo "$@"))
	@$(CC) $(CFLAGS) -I$(INC_DIR) -o $@ -c $<
	@printf "[ %d%% ] %s :: %s        \r" $(PERCENT) $(NAME) $@
	@$(eval INDEX=$(shell echo $$(($(INDEX)+1))))


obj_inc:
	@mkdir -p $(OBJ_DIR)


lib:
	@make -C libft/ --no-print-directory


clean:
	@make -C $(L_FT) clean --no-print-directory  
	@rm -f $(OBJ)
	@rm -rf $(OBJ_DIR)
	@printf '\033[33m[ KILL ] %s\n\033[0m' "Clean of $(NAME) is done ---"


fclean: 				clean
	@rm -rf $(NAME)
	@make -C $(L_FT) fclean --no-print-directory
	@printf '\033[33m[ KILL ] %s\n\033[0m' "Fclean of $(NAME) is done ---"


re:						fclean all

build:					all
	$(CC) $(CFLAGS) -iquote $(INC_DIR) -c main.c -o main.o
	$(CC) $(OFLAGS) $(OBJ) main.o $(CLIB) -o Executer

cbuild:					fclean
	rm main.o Executer

.PHONY: all clean fclean re build cbuild
